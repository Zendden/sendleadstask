<?php

use App\Entity\Lead;
use App\Service\Integration\Gateway\GoCRM\GoCRMGateway;
use App\Service\Integration\GoCRM\GoCRMLeadService;
use App\Service\Integration\LeadServiceFactory;
use App\Service\Integration\LeadServiceInterface;
use App\Service\Internal\Logger;
use App\Service\Parameters\ParameterBag;
use App\Service\Serizlier\Normalizer\ObjectNormalizer;

$_DIContainer = [
    ParameterBag::class         => (new ParameterBag()),
    Logger::class               => (new Logger()),
    GoCRMGateway::class         => (new GoCRMGateway($_DIContainer[Logger::class])),
    ObjectNormalizer::class     => (new ObjectNormalizer()),
    GoCRMLeadService::class     => (new GoCRMLeadService($_DIContainer[ParameterBag::class], $_DIContainer[GoCRMGateway::class], $_DIContainer[ObjectNormalizer::class]))
];

/**
 * @var Lead $lead
 */
$lead = (new Lead())
    ->setName((string)$_POST['name'])
    ->setPhone((string)$_POST['phone']);

/**
 * @var LeadServiceInterface $leadService
 */
$leadService = (new LeadServiceFactory($_DIContainer[GoCRMLeadService::class]))
    ->getLeadService($_DIContainer[GoCRMLeadService::class]::SERVICE_TYPE)
    ->sendLead($lead);