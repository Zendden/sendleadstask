<?php

namespace App\Entity;

class Lead
{
    /**
     * @var string
     */
    private string $name;
    
    /**
     * @var string
     */
    private string $phone;
    
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
    
    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
    
        return $this;
    }
}
