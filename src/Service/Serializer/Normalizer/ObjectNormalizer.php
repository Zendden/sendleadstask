<?php
    
namespace App\Service\Serizlier\Normalizer;

use App\Service\Serializer\Normalizer\Exception\NormalizerException;

class ObjectNormalizer implements NormalizerInterface
{
    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     *
     * @throws NormalizerException
     */
    public function normalize($object, ?string $format = null, array $context = []): array
    {
        if (
            method_exists($object, 'getName') &&
            method_exists($object, 'getPhone')
        ) {
            return [
                $object->getName(),
                $object->getPhone(),
            ];
        }
        
        throw new NormalizerException(
            sprintf('Unable to normalize object "%s", getters is not exists', get_class($object))
        );
    }
}
