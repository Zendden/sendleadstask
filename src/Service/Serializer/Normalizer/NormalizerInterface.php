<?php

namespace App\Service\Serizlier\Normalizer;
    
use App\Service\Serializer\Normalizer\Exception\NormalizerException;

interface NormalizerInterface
{
    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     *
     * @throws NormalizerException
     */
    public function normalize($object, ?string $format = null, array $context = []): array;
}
