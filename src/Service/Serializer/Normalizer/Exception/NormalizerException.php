<?php

namespace App\Service\Serializer\Normalizer\Exception;

use RuntimeException;

class NormalizerException extends RuntimeException
{
}
