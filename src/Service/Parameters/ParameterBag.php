<?php
    
namespace App\Service\Parameters;

class ParameterBag implements ParameterBagInterface
{
    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        // checking .env vars exists into container
        
        return true;
    }
    
    /**
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        // getting .env vars from container
        
        $targetValue = null;
        
        if (is_null($targetValue)) {
            return $default;
        }
    }
}
