<?php

namespace App\Service\Parameters;
    
interface ParameterBagInterface
{
    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool;
    
    /**
     * @param string      $key
     * @param mixed|null  $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null);
}
