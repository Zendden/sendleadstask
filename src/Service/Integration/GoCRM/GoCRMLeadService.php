<?php

namespace App\Service\Integration\GoCRM;

use App\Service\Integration\Gateway\GatewayInterface;
use App\Service\Parameters\ParameterBagInterface;
use App\Service\Serizlier\Normalizer\NormalizerInterface;

class GoCRMLeadService extends AbstractLeadService
{
    /**
     * @var string
     */
    public const SERVICE_TYPE = 'go-crm';
    
    /**
     * @var string
     */
    protected static $host;
    
    /**
     * @var string
     */
    protected static $key;
    
    /**
     * GoCRMLeadService constructor.
     *
     * @param ParameterBagInterface $parameterBag
     * @param GatewayInterface      $gateway
     * @param NormalizerInterface   $normalizer
     */
    public function __construct(
        ParameterBagInterface $parameterBag,
        GatewayInterface $gateway,
        NormalizerInterface $normalizer
    ) {
        parent::__construct($gateway, $normalizer);
        
        self::$host = $parameterBag->get('host', 'https://alpha.go-crm.ru');
        self::$key  = $parameterBag->get('key', '1234567890');
    }
}
