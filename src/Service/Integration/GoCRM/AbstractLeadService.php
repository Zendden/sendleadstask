<?php

namespace App\Service\Integration\GoCRM;

use App\Entity\Lead;
use App\Service\Integration\Gateway\GatewayException;
use App\Service\Integration\Gateway\GatewayInterface;
use App\Service\Integration\LeadServiceInterface;
use App\Service\Serizlier\Normalizer\NormalizerInterface;

abstract class AbstractLeadService implements LeadServiceInterface
{
    /**
     * @var string|null
     */
    public const SERVICE_TYPE = null;
    
    /**
     * @var string|null
     */
    protected static $host;
    
    /**
     * @var string|null
     */
    protected static $key;
    
    /**
     * @var GatewayInterface
     */
    protected GatewayInterface $gateway;
    
    /**
     * @var NormalizerInterface
     */
    protected NormalizerInterface $normalizer;
    
    /**
     * AbstractLeadService constructor.
     *
     * @param GatewayInterface    $gateway
     * @param NormalizerInterface $normalizer
     */
    public function __construct(GatewayInterface $gateway, NormalizerInterface $normalizer)
    {
        $this->gateway      = $gateway;
        $this->normalizer   = $normalizer;
    }
    
    /**
     * This is a base method and should be overridden in a child class if necessary.
     * This method provides basic functionality for sending Leads to CRM.
     *
     * @param Lead $lead
     *
     * @return int
     *
     * @throws GatewayException
     */
    public function sendLead(Lead $lead): int
    {
        return $this->gateway
            ->setHost(static::$host)
            ->setKey(static::$key)
            ->sendData($this->normalizer->normalize($lead));
    }
}
