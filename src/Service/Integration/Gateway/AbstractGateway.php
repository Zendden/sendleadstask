<?php

namespace App\Service\Integration\Gateway;

use App\Service\Internal\LoggerInterface;
use JsonException;

abstract class AbstractGateway implements GatewayInterface
{
    /**
     * @var string|null
     */
    protected static ?string $host;
    
    /**
     * @var string|null
     */
    protected static ?string $key;
    
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;
    
    /**
     * AbstractGateway constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * @param array $data
     *
     * @return array
     *
     * @throws GatewayException
     * @throws JsonException
     */
    public function request(array $data): array
    {
        $this->logger->info(
            json_encode(
                [
                    'requestData'   => $data,
                    'host'          => static::$host,
                ],
                JSON_THROW_ON_ERROR
            )
        );
    
        /** Pseudocode: $response = parent::request($data, static::$host, static::$key); */
        
        /**
         * Response from a remote service.
         */
        $response = [
            'code'      => 200,
            'data'      => 1,
            'message'   => '',
        ];
        
        if ($response['code'] !== 200) {
            $errorMessage = sprintf(
                'Request failed. Message: "%s", StatusCode: "%s"',
                $response['message'],
                $response['code']
            );
            
            $this->logger->error(
                json_encode(
                    [
                        'requestData'   => $data,
                        'host'          => static::$host,
                        'message'       => $errorMessage
                    ],
                    JSON_THROW_ON_ERROR
                )
            );
            
            throw new GatewayException($errorMessage);
        }
        
        return $response;
    }

    /**
     * @param string $host
     *
     * @return GatewayInterface
     */
    abstract public function setHost(string $host): GatewayInterface;
    
    /**
     * @param string $key
     *
     * @return GatewayInterface
     */
    abstract public function setkey(string $key): GatewayInterface;
}
