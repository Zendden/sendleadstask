<?php

namespace App\Service\Integration\Gateway;
    
interface GatewayInterface
{
    /**
     * @param array $data
     *
     * @return int
     *
     * @throws GatewayException
     */
    public function sendData(array $data): int;
    
    /**
     * @param string $host
     *
     * @return $this
     */
    public function setHost(string $host): self;
    
    /**
     * @param string $key
     *
     * @return $this
     */
    public function setkey(string $key): self;
}
