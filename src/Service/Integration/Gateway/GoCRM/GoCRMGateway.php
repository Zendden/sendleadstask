<?php

namespace App\Service\Integration\Gateway\GoCRM;
    
use App\Service\Integration\Gateway\AbstractGateway;
use App\Service\Integration\Gateway\GatewayInterface;
use JsonException;
use Throwable;

class GoCRMGateway extends AbstractGateway
{
    /**
     * @var string|null
     */
    protected static ?string $host;
    
    /**
     * @var string|null
     */
    protected static ?string $key;
    
    /**
     * @param array $data
     *
     * @return int
     */
    public function sendData(array $data): int
    {
        try {
            $response = $this->request($data);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
            
            return 0;
        }
        
        /** Returning a data from response or error code. */
        return $response['data'] ?? 0;
    }
    
    /**
     * @param string $host
     *
     * @return GatewayInterface
     */
    public function setHost(string $host): GatewayInterface
    {
        self::$host = $host;
    }
    
    /**
     * @param string $key
     *
     * @return GatewayInterface
     */
    public function setkey(string $key): GatewayInterface
    {
        self::$key = $key;
    }
}
