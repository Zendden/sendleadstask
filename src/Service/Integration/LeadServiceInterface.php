<?php

namespace App\Service\Integration;

use App\Entity\Lead;
    
interface LeadServiceInterface
{
    /**
     * @param Lead $lead
     *
     * @return int
     */
    public function sendLead(Lead $lead): int;
}
