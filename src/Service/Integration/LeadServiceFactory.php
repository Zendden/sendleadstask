<?php
    
namespace App\Service\Integration;

use App\Service\Integration\GoCRM\GoCRMLeadService;
use LogicException;

class LeadServiceFactory
{
    /**
     * @var GoCRMLeadService
     */
    private GoCRMLeadService $goCRMLeadService;
    
    /**
     * LeadServiceFactory constructor.
     *
     * @param GoCRMLeadService $goCRMLeadService
     */
    public function __construct(GoCRMLeadService $goCRMLeadService)
    {
        $this->goCRMLeadService = $goCRMLeadService;
    }
    
    /**
     * @param string $type
     *
     * @return LeadServiceInterface
     */
    public function getLeadService(string $type): LeadServiceInterface
    {
        $services = $this->getServices();
        
        if (!array_key_exists($type, $services)) {
            throw new LogicException(
                sprintf('Unable to get service, passed undefined type "%s".', $type)
            );
        }
        
        return $services[$type];
    }
    
    /**
     * @return LeadServiceInterface[]|GoCRMLeadService[]|array
     */
    private function getServices(): array
    {
        return [
            $this->goCRMLeadService::SERVICE_TYPE => $this->goCRMLeadService,
            // ..., more services
        ];
    }
}
