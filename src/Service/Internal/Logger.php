<?php

namespace App\Service\Internal;

class Logger implements LoggerInterface
{
    /**
     * @param string $text
     */
    public function info(string $text): void
    {
        // info log
    }
    
    /**
     * @param string $text
     */
    public function note(string $text): void
    {
        // note log
    }
    
    /**
     * @param string $text
     */
    public function warning(string $text): void
    {
        // warning log
    }
    
    /**
     * @param string $text
     */
    public function error(string $text): void
    {
        // error log
    }
}
