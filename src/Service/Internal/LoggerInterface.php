<?php
    
namespace App\Service\Internal;
    
interface LoggerInterface
{
    /**
     * @param string $text
     */
    public function info(string $text): void;
    
    /**
     * @param string $text
     */
    public function note(string $text): void;
    
    /**
     * @param string $text
     */
    public function warning(string $text): void;
    
    /**
     * @param string $text
     */
    public function error(string $text): void;
}
